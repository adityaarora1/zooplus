package com.example.adityaarora.zooplus.api;

import com.example.adityaarora.zooplus.models.Order;
import com.example.adityaarora.zooplus.models.ProductList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ProductService {

    @GET("products")
    Call<ProductList> getProducts();

    @PUT("order")
    Call<Order> placeOrder();
}
