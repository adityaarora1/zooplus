package com.example.adityaarora.zooplus.interfaces;


import com.example.adityaarora.zooplus.models.Product;

public interface OnItemClickListener {
    void onItemClick(Product product);
}