package com.example.adityaarora.zooplus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adityaarora.zooplus.PaginationAdapter;
import com.example.adityaarora.zooplus.R;
import com.example.adityaarora.zooplus.api.ProductService;
import com.example.adityaarora.zooplus.api.RestApi;
import com.example.adityaarora.zooplus.fragments.ProductDetailsFragment;
import com.example.adityaarora.zooplus.interfaces.CartInterface;
import com.example.adityaarora.zooplus.interfaces.OnItemClickListener;
import com.example.adityaarora.zooplus.models.Product;
import com.example.adityaarora.zooplus.models.ProductList;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements OnItemClickListener, CartInterface {
    ArrayList<Product> cartList = new ArrayList<>();

    private static final String TAG = "MainActivity";

    PaginationAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    RecyclerView rv;
    ProgressBar progressBar;

    private ProductService productService;
    private TextView viewCartTV;
    List<Product> products;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        rv = findViewById(R.id.main_recycler);
        progressBar = findViewById(R.id.main_progress);
        viewCartTV = findViewById(R.id.view_cart);

        adapter = new PaginationAdapter(this, this);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);

        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        //init service and load data
        productService = RestApi.getClient().create(ProductService.class);

        viewCartTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cartList.isEmpty()) {
                    Intent intent = new Intent(HomeActivity.this, CartActivity.class);
                    intent.putExtra("CART_DATA", cartList);
                    startActivity(intent);
                } else {
                    Toast.makeText(HomeActivity.this, "Your cart is empty.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        loadFirstPage();
    }


    private void loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ");

        callGetProductsApi().enqueue(new Callback<ProductList>() {
            @Override
            public void onResponse(Call<ProductList> call, Response<ProductList> response) {
                products = fetchProducts(response);
                progressBar.setVisibility(View.GONE);
                adapter.addAll(products);
            }

            @Override
            public void onFailure(Call<ProductList> call, Throwable t) {
                products = createDataSet("products_json_1.json").getProductArrayList();
                progressBar.setVisibility(View.GONE);
                adapter.addAll(products);
            }
        });
    }

    /**
     * @param response extracts List<{@link Product >} from response
     * @return
     */
    private List<Product> fetchProducts(Response<ProductList> response) {
        ProductList productList = response.body();
        return productList.getProductArrayList();
    }

    /**
     * Performs a Retrofit call to the get products API.
     */
    private Call<ProductList> callGetProductsApi() {
        return productService.getProducts();
    }

    @Override
    public void onItemClick(Product product) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        ProductDetailsFragment productDetailsFragment = ProductDetailsFragment.newInstance(product);
        productDetailsFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
        productDetailsFragment.show(mFragmentManager, "");
    }

    private String loadJsonFromAssets(String fileName) {
        String json = null;
        try {
            InputStream inputStream = getAssets().open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return json;
    }

    private ProductList createDataSet(String fileName) {
        return new Gson().fromJson(loadJsonFromAssets(fileName), ProductList.class);
    }

    @Override
    public void addToCart(Product product) {
        boolean found = false;
        for (Product prod: cartList) {
            if(prod.getId() == product.getId()) {
                prod.setQty(prod.getQty() + 1);
                found = true;
            }
        }

        if(!found)
            cartList.add(product);
        Toast.makeText(HomeActivity.this, "Added to cart", Toast.LENGTH_SHORT).show();
    }
}