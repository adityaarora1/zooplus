package com.example.adityaarora.zooplus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.adityaarora.zooplus.interfaces.OnItemClickListener;
import com.example.adityaarora.zooplus.interfaces.RecyclerViewClickListener;
import com.example.adityaarora.zooplus.models.Product;
import com.example.adityaarora.zooplus.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class PaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnItemClickListener onItemCLickListener;
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Product> productResults;
    private Context context;

    private boolean isLoadingAdded = false;

    public PaginationAdapter(Context context, OnItemClickListener onItemCLickListener) {
        this.context = context;
        productResults = new ArrayList<>();
        this.onItemCLickListener = onItemCLickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.item_list, parent, false);
        viewHolder = new ProductVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Product product = productResults.get(position); // Product

        switch (getItemViewType(position)) {
            case ITEM:
                final ProductVH productVH = (ProductVH) holder;

                productVH.name.setText(product.getName());


                if (product.getQty() > 1) {
                    productVH.price.setText(
                            product.getPrice() // we want the year only
                                    + " * "
                                    + product.getQty() + " = "
                                    + Math.round(product.getQty() * product.getPrice()) + " "
                                    + product.getCurrency().toUpperCase()
                    );
                } else {
                    productVH.price.setText(
                            product.getPrice() // we want the year only
                                    + " "
                                    + product.getCurrency().toUpperCase()
                    );
                }
                Glide
                        .with(context)
                        .load(Utils.getImage(product.getImage_base_url(), context))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                        .centerCrop()
                        .crossFade()
                        .into(productVH.img);

                ((ProductVH) holder).setRecyclerViewClickListener(new RecyclerViewClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        if(null != onItemCLickListener)
                            onItemCLickListener.onItemClick(product);
                    }
                });

                if(null != onItemCLickListener) {
                    productVH.arrowImg.setVisibility(View.VISIBLE);
                } else {
                    productVH.arrowImg.setVisibility(View.GONE);
                }
                break;

            case LOADING:
//                Do nothing
                break;
        }
    }


    @Override
    public int getItemCount() {
        return productResults == null ? 0 : productResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == productResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }
    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(Product r) {
        productResults.add(r);
        notifyItemInserted(productResults.size() - 1);
    }

    public void addAll(List<Product> products) {
        for (Product result : products) {
            add(result);
        }
    }

   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class ProductVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView arrowImg;
        private TextView name;
        private TextView price; // displays "year | language"
        private ImageView img;
        private ProgressBar mProgress;
        private RecyclerViewClickListener mListener;

        public ProductVH(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.product_name);
            price = itemView.findViewById(R.id.product_price);
            img = itemView.findViewById(R.id.product_img);
            mProgress = itemView.findViewById(R.id.progress);
            arrowImg = itemView.findViewById(R.id.arrow_img);

            itemView.setOnClickListener(this);
        }

        public void setRecyclerViewClickListener(RecyclerViewClickListener recyclerViewClickListener) {
            this.mListener = recyclerViewClickListener;
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
