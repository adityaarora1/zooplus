package com.example.adityaarora.zooplus.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.adityaarora.zooplus.PaginationAdapter;
import com.example.adityaarora.zooplus.R;
import com.example.adityaarora.zooplus.api.ProductService;
import com.example.adityaarora.zooplus.api.RestApi;
import com.example.adityaarora.zooplus.interfaces.CartInterface;
import com.example.adityaarora.zooplus.models.Order;
import com.example.adityaarora.zooplus.models.Product;
import com.example.adityaarora.zooplus.models.ProductList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    PaginationAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    RecyclerView rv;
    ProgressBar progressBar;

    private ArrayList<Product> cartList;
    private ImageView orderCart;
    private ProductService productService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        rv = findViewById(R.id.main_recycler);
        progressBar = findViewById(R.id.main_progress);
        orderCart = findViewById(R.id.order_cart);

        adapter = new PaginationAdapter(this, null);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);

        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        productService = RestApi.getClient().create(ProductService.class);

        cartList = (ArrayList<Product>) getIntent().getSerializableExtra("CART_DATA");

        orderCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderCartData();
            }
        });

        loadData();
    }

    private void orderCartData() {
        callOrderProductsApi().enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                Toast.makeText(CartActivity.this,
                        "Your order id is " + response.body().getOrderId(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(CartActivity.this,
                        "Failed to oder, please try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Performs a Retrofit call to the order products API.
     */
    private Call<Order> callOrderProductsApi() {
        return productService.placeOrder();
    }

    private void loadData() {
        progressBar.setVisibility(View.GONE);
        adapter.addAll(cartList);
    }
}