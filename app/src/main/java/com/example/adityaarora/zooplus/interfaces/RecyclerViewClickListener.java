package com.example.adityaarora.zooplus.interfaces;
import android.view.View;

public interface RecyclerViewClickListener {

    void onClick(View view, int position);
}