package com.example.adityaarora.zooplus.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {
    public static int getImage(String imageName, Context context) {
        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable",
                context.getPackageName());
        return drawableResourceId;
    }
}
