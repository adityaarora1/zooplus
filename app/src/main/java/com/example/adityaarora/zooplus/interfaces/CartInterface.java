package com.example.adityaarora.zooplus.interfaces;

import com.example.adityaarora.zooplus.models.Product;

public interface CartInterface {
    void addToCart(Product product);
}
