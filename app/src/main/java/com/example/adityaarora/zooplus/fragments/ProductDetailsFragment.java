package com.example.adityaarora.zooplus.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.adityaarora.zooplus.R;
import com.example.adityaarora.zooplus.interfaces.CartInterface;
import com.example.adityaarora.zooplus.models.Product;
import com.example.adityaarora.zooplus.utils.Utils;

import static android.content.Context.MODE_PRIVATE;

public class ProductDetailsFragment extends android.support.v4.app.DialogFragment {
    private static final String SELECTED_PRODUCT = "product_selected";
    private static final String CART_PREFS_NAME = "cart_pref";
    private Product product;
    private ImageView image;
    private TextView title;
    private TextView date;
    private View cartImg;
    private CartInterface cartInterface;

    public static ProductDetailsFragment newInstance(Product product) {
        ProductDetailsFragment productDetailsFragment = new ProductDetailsFragment();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(SELECTED_PRODUCT, product);
        productDetailsFragment.setArguments(mBundle);
        return productDetailsFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog mDialog = super.onCreateDialog(savedInstanceState);
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return mDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.product_detail, container, false);

        if (null != getArguments()) {
            product = (Product) getArguments().getSerializable(SELECTED_PRODUCT);
        }
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = view.findViewById(R.id.image);
        title = view.findViewById(R.id.title);
        date = view.findViewById(R.id.date);
        cartImg = view.findViewById(R.id.add_to_cart_img);
        Glide
                .with(getActivity())
                .load(Utils.getImage(product.getImage_base_url(), getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                .centerCrop()
                .crossFade()
                .into(image);
        title.setText(product.getName().trim());
        date.setText(String.valueOf(product.getPrice() + " " + product.getCurrency()));
        cartImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartInterface.addToCart(product);
                dismissAllowingStateLoss();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            cartInterface = (CartInterface) context;
        }  catch (ClassCastException e) {
        throw new ClassCastException(context.toString()
                + " must implement CartInterface");
    }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cartInterface = null;
    }
}
