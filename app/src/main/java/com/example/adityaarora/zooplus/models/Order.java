package com.example.adityaarora.zooplus.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Order implements Serializable {
    public int getOrderId() {
        return orderId;
    }

    @SerializedName("order_id")
    private int orderId;
}
