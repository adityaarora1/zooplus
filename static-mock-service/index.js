var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('static zooplus service mocks. possible calls: GET /products PUT /orders');
});

app.get('/products', function (req, res) {
//maybe externalize to file
  res.send('{"products":['+
  '{"id":"3141", "name":"PLA Rinti Kennerfleisch Gefluegelherzen 800g", "price":"2.95", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
    '{"id":"7526", "name":"PLA Rinti Kennerfleisch Seefisch 800g", "price":"3.99", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
      '{"id":"11887", "name":"PLA Felix Leckerbissen in Sosse Kaninchen Huhn 400g", "price":"19.95", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
        '{"id":"12248", "name":"wow_dose 800g arcticspirit", "price":"1.15", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
          '{"id":"12249", "name":"wow dose 800g greenfields", "price":"10.02", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
            '{"id":"28041", "name":"28041 pla rocco sensitive huhn 6x 800g", "price":"11.01", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
             '{"id":"31085", "name":"pla animonda grancarn", "price":"12.22", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
              '{"id":"51197", "name":"pla animonda multifle", "price":"13.03", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"},'+
            '{"id":"28015", "name":"pla rocco classic rind huhn 6x 800g", "price":"999.99", "currency":"EUR", "image_base_url":"media.zooplus.de/images/products/"}'+']}');
});

app.post('/products', function (req, res) {
//maybe externalize to file
	res.status('400');
	res.send('products cannot be modified!');
});

app.put('/products', function (req, res) {
//maybe externalize to file
	res.status('400');
	res.send('cannot create product!');
});


app.put('/orders', function (req, res){
	var rnd = 0;
	while (rnd < 50){
		rnd = Math.floor((Math.random()*1000)) % 400;
	}
	
	if(rnd % 29 === 0){
		res.status('500');
		res.send('internal server error.');
		console.log ('the service failed intentionally, try again!');
	}else{
		res.status('200');
		res.send('{"order_id":"'+rnd+'"}');
		console.log ('order received: order id=' + rnd);
	}
});

app.post('/orders', function (req, res){
	res.status('400');
	res.send('orders cannot be modified!');
});

app.listen(3000, function () {
  console.log('started zooplus static service on port 3000!');
});
